# Regenerate Yarn lockfile

```bash
npx yarn
```

# Usage

## Read profile

```bash
export SOLID_USERNAME=agentydragon
export SOLID_PASSWORD=<...>

bazel run :solid_cli -- \
  --command=readfile \
  --url='https://agentydragon.solidcommunity.net/profile/card#me'
```

## Create dir, write, read, delete

```bash
BASE_URL=https://agentydragon.solidcommunity.net/private/blahtest/cli-test

bazel run :solid_cli -- --command=createfolder --url=${BASE_URL}

bazel run :solid_cli -- \
  --command=createfile \
  --url=${BASE_URL}/file.txt \
  --mimetype=text/plain \
  --filepath=<(echo "Hello World")

bazel run :solid_cli -- --command=readfile --url=${BASE_URL}/file.txt

bazel run :solid_cli -- --command=deletefile --url=${BASE_URL}/file.txt

bazel run :solid_cli -- --command=deletefolder --url=${BASE_URL}
```
