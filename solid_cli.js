const flags = require('flags');
const fs = require('fs');
const process = require('process');
const solidAuthCli = require('solid-auth-cli');
const solidFileClient = require('solid-file-client');
const util = require('util');

const fc = new solidFileClient(solidAuthCli);

flags.defineString('idp', 'https://solidcommunity.net', 'Identity provider');
flags.defineString(
    'username', null, 'Username; otherwise taken from SOLID_USERNAME');
flags.defineString(
    'password', null, 'Password; otherwise taken from SOLID_PASSWORD');
flags.defineString('command', null, 'Command');
flags.defineString('url', null, 'URL');
flags.defineString('mimetype', null, 'MIME type');
flags.defineString('filepath', null, 'Path to new uploaded file');

async function readFile(url) {
  const content = await fc.readFile(url);
  process.stdout.write(content);
}

async function createFile(url, filepath, mimetype) {
  const readFile = util.promisify(fs.readFile);
  const fileContent = await readFile(filepath, 'utf8');
  await fc.createFile(url, fileContent, mimetype, {});
}

async function createFolder(url) {
  await fc.createFolder(url, {});
}

async function deleteFolder(url) {
  await fc.deleteFolder(url, {});
}

async function deleteFile(url) {
  await fc.deleteFile(url);
  console.log('file was deleted: ', url);
}

async function run() {
  flags.parse();

  let session = await solidAuthCli.currentSession()
  if (!session) {
    session = await solidAuthCli.login({
      'idp': flags.get('idp'),
      'username': flags.get('username') || process.env['SOLID_USERNAME'],
      'password': flags.get('password') || process.env['SOLID_PASSWORD']
    });
  }
  // console.log(`Logged in as ${session.webId}.`);

  switch (flags.get('command')) {
    case 'readfile':
      await readFile(flags.get('url'));
      break;
    case 'createfile':
      await createFile(
          flags.get('url'), flags.get('filepath'), flags.get('mimetype'));
      break;
    case 'createfolder':
      await createFolder(flags.get('url'));
      break;
    case 'deletefile':
      await deleteFile(flags.get('url'));
      break;
    case 'deletefolder':
      await deleteFolder(flags.get('url'));
      break;
    default:
      console.log('unknown command ' + flags.get('command'));
      break;
  }

  // TODO: const client = new SolidNodeClient();

  // with RDFlib:
  //    import {SolidNodeClient} from 'solid-node-client';
  //    import * as $rdf from 'rdflib';
  //    const client = new SolidNodeClient();
  //    const store   = $rdf.graph();
  //    const fetcher = $rdf.fetcher(store,{fetch:client.fetch.bind(client)});
  //        // now all rdflib methods support file:// requests in nodejs
  //    async function main(){
  //        let session = await login();  // may be omitted if you don't need
  //        authentication
  //        // now all rdflib methods support authenticated http: requests in
  //        nodejs
  //    }

  // let response = await client.fetch('https://example.com/publicResource');
  // console.log(await response.text())
  // let session = await client.login();
  // if( session ) {
  //    console.log (`logged in as <${session.webId}>`);
  //    response = await client.fetch('https://example.com/privateResource');
  //    console.log(await response.text())
  //    logout();
  //}
}


// also exists:
//   deleteUrl, deleteFolder

run();
